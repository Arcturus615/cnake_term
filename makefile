main.o: main.c
	gcc -o out main.c -lncurses

debug: main.c
	gcc -o out -g3 main.c -lncurses 

clean:
	rm -rf build
