#include <stdio.h>
#include <string.h>
#include <ncurses.h>

#define DEBUG 0
#define DEBUGLEVEL 2
#define MAXX 100
#define MAXY 50

struct Player
{
	int x;
	int y;
};

struct Session
{
	bool running;
	char buffer[MAXY][MAXX];
};

// refresh: Reset char buffer passed to 0
int refresh_buf ( char buffer[MAXY][MAXX], int max_x, int max_y );


int main ( int argc, char **argv )
{
	char c;

	struct Session game;
	struct Player plr;

	game.running = true;
	plr.x = MAXX/2;
	plr.y = MAXY/2;

	while ( game.running == true )
	{
		if ( DEBUG == true )
			printf( "PLR Coords: (%d, %d)\n", plr.x, plr.y );
		
		refresh_buf( game.buffer, MAXX, MAXY );
	}

	return 0;

}

int refresh_buf ( char buf[MAXY][MAXX], int mx, int my)
{
	if ( DEBUG == true && DEBUGLEVEL >= 2 )
		printf( "Resetting Buffer at Addr: %p\n", buf );	
	
	memset( buf, 0, sizeof(buf[0][0]) * mx * my);
	return 0;
}

